dinifti
=======

The dinifti program converts MRI images stored in DICOM format to
NIfTI format.

For installation information see the INSTALL file.

Distribution
------------

dinifti is licensed under the GNU General Public License (GPL), version 2. See
COPYING.

Usage
=====

The program can convert single files, a list of files or a whole directory. If
any of the input files are not in DICOM format, an error message will be
printed, but the program will continue. 

Usage: dinifti [OPTION] <DICOM input> <NIfTI output>

Options:
 -g                   compressed output
 -f <output format>   'a2' - ANALYZE 7.5 Dual file
                      'n2' - NIfTI-2 Dual file
                      'n1' - NIfTI-1 Single file **Default**
 -d                   append series description to output file name(s)

 -h --help       print this help and exit
 -v --version    print version number and exit

I/O Options
<DICOM input> can be single file, list of files or directory
<NIfTI output> can be single file or directory

