dicomnifti (2.33.1-5) unstable; urgency=medium

  * Team upload.
  * d/t/control : disable tests on s390,i386 (Closes: #1013364).

 -- Mohammed Bilal <mdbilal@disroot.org>  Wed, 20 Jul 2022 19:14:05 +0530

dicomnifti (2.33.1-4) unstable; urgency=medium

  * Team upload.
  * Add autopkgtests
  * Install examples

 -- Mohammed Bilal <mdbilal@disroot.org>  Mon, 30 May 2022 13:54:03 +0530

dicomnifti (2.33.1-3) unstable; urgency=medium

  * Team upload.
  * DEP5 copyright
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 10 Nov 2021 17:35:03 +0100

dicomnifti (2.33.1-2) unstable; urgency=low

  * Team upload

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set debhelper-compat version in Build-Depends.

  [ Andreas Tille ]
  * Move package to Debian Med team
  * Build-Depends: s/libnifti-dev/libnifti2-dev
  * Remove default debian/gbp.conf
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * watch file standard 4 (routine-update)
  * Enable dh_missing --fail-missing
  * Remove invalid lintian-override

 -- Andreas Tille <tille@debian.org>  Mon, 07 Dec 2020 15:14:41 +0100

dicomnifti (2.33.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Wed, 26 Sep 2018 10:55:46 +0200

dicomnifti (2.32.1-1) unstable; urgency=medium

  * New upstream release.
  * Bumped Standards-version to 3.9.5, no changes necessary.

 -- Michael Hanke <mih@debian.org>  Thu, 02 Oct 2014 12:57:34 +0200

dicomnifti (2.31.1-1) unstable; urgency=low

  * New upstream release.
  * Bumped debhelper comput level to 9 to enable hardening.
  * Bumped Standards-version to 3.9.4, no changes necessary.

 -- Michael Hanke <mih@debian.org>  Mon, 29 Jul 2013 20:38:02 +0200

dicomnifti (2.30.0-1) unstable; urgency=low

  * New upstream release. Output NIfTI images now have slope and intercept
    set accordingly in their headers.

 -- Michael Hanke <mih@debian.org>  Sun, 01 Apr 2012 20:14:52 +0200

dicomnifti (2.29.1-1) unstable; urgency=low

  * New upstream release.

 -- Michael Hanke <mih@debian.org>  Mon, 24 Oct 2011 11:08:54 +0200

dicomnifti (2.28.16-1) unstable; urgency=low

  * New upstream release.
  * Bumped Standards-version to 3.9.2, no changes necessary.
  * Updated debian/copyright.

 -- Michael Hanke <mih@debian.org>  Mon, 15 Aug 2011 13:01:47 +0200

dicomnifti (2.28.15-1) unstable; urgency=low

  * New upstream release.
  * Updated maintainer email address.
  * Bumped Standards-version to 3.9.1, no changes necessary.
  * Add lintian override regarding outdated autotools files. The package only
    uses cmake to build the binaries.

 -- Michael Hanke <mih@debian.org>  Tue, 18 Jan 2011 17:21:08 -0500

dicomnifti (2.28.14-2) unstable; urgency=low

  * Changed maintainer to NeuroDebian Team.
  * Switch from plain debhelper to 'dh'.
  * Convert source package format to 3.0 (quilt).
  * Bumped Standards-version to 3.9.0, no changes necessary.
  * Add patch to prevent unnecessary package dependency in zlib.

 -- Michael Hanke <michael.hanke@gmail.com>  Tue, 20 Jul 2010 17:12:46 -0400

dicomnifti (2.28.14-1) unstable; urgency=low

  * New upstream version.
  * Updated Debian Standards-version to 3.8.1 (no changes necessary).
  * Added 'XS-DM-Upload-Allowed: yes' to debian/control.

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 08 Apr 2009 12:12:07 +0200

dicomnifti (2.28.11-1) unstable; urgency=low

  * New upstream version. Fix for GCC 4.3 compatibility (Closes: #462074).

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 24 Jan 2008 19:43:26 +0100

dicomnifti (2.28.10-2) unstable; urgency=low

  * Modified build-deps for libnifti transition.
  * Bumped Standards-version to 3.7.3, no changes necessary.

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 03 Jan 2008 12:09:30 +0100

dicomnifti (2.28.10-1) unstable; urgency=low

  * New Upstream Version.
  * Remove patch to prevent fixed sizes for 'int' and 'long' as upstream
    switched to determine this with CMake dynamically (although it is still
    not necessary for Debian's CTN library).

 -- Michael Hanke <michael.hanke@gmail.com>  Fri, 30 Nov 2007 10:59:00 +0100

dicomnifti (2.28.8-1) unstable; urgency=low

  * New Upstream Version. Fixes a bug where dinifti did not respect the users
    choice of number of slices (option -s).

 -- Michael Hanke <michael.hanke@gmail.com>  Sun, 11 Nov 2007 11:20:07 +0100

dicomnifti (2.28.7-1) unstable; urgency=low

  * New Upstream Version. Changed license to a 3-clause BSD license.

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 07 Nov 2007 09:38:35 +0100

dicomnifti (2.28.5-2) unstable; urgency=low

  * Updated Homepage info in debian/control using the new 'Homepage' field.
  * Added VCS info to debian/control.

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 18 Oct 2007 20:22:40 +0200

dicomnifti (2.28.5-1) unstable; urgency=low

  * New Upstream Version. Allows for custom naming schemes of image series.
    (Closes: #446260)

 -- Michael Hanke <michael.hanke@gmail.com>  Tue, 16 Oct 2007 00:46:55 +0200

dicomnifti (2.28.4-1) unstable; urgency=low

  * New Upstream bugfix release.
  * Siemens Vision: fails to read the number of slices, now defaults 1
    and user can override this with the '-s' option (Closes: #442013).
  * GE Elscint: fails to read series' description, only needed with
     '-d' flag. Now defaults to ""
  * Build-depend on libniftiio1-dev by default.

 -- Michael Hanke <michael.hanke@gmail.com>  Mon, 17 Sep 2007 10:38:18 +0200

dicomnifti (2.28.2-1) unstable; urgency=low

  * New upstream release. NIfTI header value 'nu' is now always set to one
    instead of the number of channels reported in the DICOMs.
  * Updated upstream URL in manpages.

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 12 Sep 2007 14:08:57 +0200

dicomnifti (2.28-1) unstable; urgency=low

  * New upstream release. Upstream switched from custom makefiles to
    CMake-based build system.
  * Converted NIfTI files now contain SPM5-like acquisition information.
    (Closes: #431591)
  * Bugfix: Wrong origin when converting sagital images (e.g. Siemens MPRAGE).
    (Closes: #431652)
  * Binaries are now build with -Wall -g -O2 by default (as it should be).
  * debian/rules honours DEB_BUILD_OPTIONS='noopt' and sets -O0 accordingly.
  * Removed dpatch build-dependency in favor of Git.
  * Fixed member init order in dicomInfo class.

 -- Michael Hanke <michael.hanke@gmail.com>  Sun, 15 Jul 2007 20:59:07 +0200

dicomnifti (2.27-1) unstable; urgency=low

  * New upstream release. Fix image pixel location (half-pixel shift) to
    properly convert Siemens DICOMs.

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 24 May 2007 06:45:08 +0200

dicomnifti (2.26.1-1) unstable; urgency=low

  * New upstream release.
  * Merged all non-Debian-specific patches with upstream. This also fixes the
    pending GCC 4.3 header issue that should have been fixed by the last
    upload, but wasn't as the included patch was disabled by mistake.
    (Closes: #417157)
  * Fixed bug: wrong header information (dimensions, etc) (Closes: #420019)

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 25 Apr 2007 18:25:23 +0200

dicomnifti (2.25-2) unstable; urgency=low

  * Added patch to check for duplicate image series names. dinifti now
    modifies the series name and issues a warning instead of silently
    overwriting the file.
  * Added patch to add a --noact option to be able to quickly inspect the
    content of a DICOM directory without having to actually write the NIfTI
    files.
  * Documented the new behavior and options in the manpage.
  * Added patch to fix a missing header dependency which would result in a
    FTBFS with the coming GCC 4.3. Thanks to Martin Michlmayr for providing
    the patch. (Closes: #417157)

 -- Michael Hanke <michael.hanke@gmail.com>  Thu,  5 Apr 2007 15:27:38 +0200

dicomnifti (2.25-1) unstable; urgency=low

  * New upstream release. Fixes a bug when converting MOSAIC images with a
    rectangular FOV.

 -- Michael Hanke <michael.hanke@gmail.com>  Sat, 17 Mar 2007 07:47:01 +0100

dicomnifti (2.24-1) unstable; urgency=low

  * New upstream release. Fixes a problem when converting DICOMs from 3T GE
    scanners.

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 22 Feb 2007 08:11:08 +0100

dicomnifti (2.23-1) unstable; urgency=low

  * New upstream release. Fixes a bug that caused dinifti to crash if not all
    DICOMs of an image series are available.
  * Merged most patches with upstream. Thanks to Valerio Luccio.
  * Added an additional space to the Homepage field in debian/control.
  * Updated debian/copyright (year in the copyright statement).

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 21 Feb 2007 10:20:24 +0100

dicomnifti (2.22-2) unstable; urgency=low

  * Fixed a number of memory leaks. I'm grateful to Yaroslav Halchenko
    for pointing to a nasty STL-related bug. (Closes: #394834)
  * Build-depend on ctn-dev >> 3.0.6-9 to finally solve the datatype size
    issues on 64bit systems (see #387183).
  * Added upstream homepage to the package description.

 -- Michael Hanke <michael.hanke@gmail.com>  Sun, 28 Jan 2007 19:24:44 +0100

dicomnifti (2.22-1) unstable; urgency=low

  * New upstream release.
  * Patches merged with upstream.

 -- Michael Hanke <michael.hanke@gmail.com>  Thu,  5 Oct 2006 19:49:23 +0200

dicomnifti (2.21-3) unstable; urgency=low

  * Added --verbose option to enable a more verbose status output. This is
    mostly useful to associate dinifti warnings with certain image series,
    when converting directories containing multiple series.

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 21 Sep 2006 19:50:56 +0200

dicomnifti (2.21-2) unstable; urgency=low

  * Added dependency to autotools-dev to get fresh copies of config.sub and
    config.guess. This fixes a FTBFS on AMD64.
  * Possible silent overwrite of converted files is noted in the manpage.

 -- Michael Hanke <michael.hanke@gmail.com>  Mon,  4 Sep 2006 12:57:25 +0200

dicomnifti (2.21-1) unstable; urgency=low

  * New upstream release.

 -- Michael Hanke <michael.hanke@gmail.com>  Tue, 29 Aug 2006 07:43:13 +0200

dicomnifti (2.20-1) unstable; urgency=low

  * New upstream release.
  * Added minimal manpage for dicomhead, enhanced manpage for dinifti. Both
    are forwarded to upstream and will become part of the next upstream
    release.

 -- Michael Hanke <michael.hanke@gmail.com>  Mon, 28 Aug 2006 16:11:43 +0200

dicomnifti (2.11-1) unstable; urgency=low

  * Initial release. (Closes: #383563)

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 16 Aug 2006 02:11:44 +0200
