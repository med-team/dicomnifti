cmake_minimum_required(VERSION 2.6)
if(COMMAND cmake_policy)
  cmake_policy(SET CMP0003 NEW)
endif(COMMAND cmake_policy)
# Define project name and identify as C++
PROJECT(DINIFTI CXX C)

# Configure man dir (but hide option for regular user)
SET( DINIFTI_MAN_DIR man/man1 CACHE PATH "Directory where manpages will be installed." )
MARK_AS_ADVANCED( DINIFTI_MAN_DIR )

# Configure toplevel directories
SET( PREFIX     ${CMAKE_INSTALL_PREFIX} CACHE PATH "Top level.")
SET( INCLUDEDIR ${PREFIX}/include       CACHE PATH "Include files.")
SET( LIBDIR     ${PREFIX}/lib           CACHE PATH "Libraries.")

# Configure CTN
FIND_PATH( DINIFTI_CTN_INCLUDE dicom.h 
           PATHS ${INCLUDEDIR}
           PATH_SUFFIXES ctn
           DOC "Path to the CTN headers." )
FIND_LIBRARY( DINIFTI_CTN_LIB ctn 
              PATHS ${LIBDIR}
              PATH_SUFFIXES ctn
              DOC "Location of the CTN library." )

# Configure NIfTI (+ znzlib)
FIND_PATH( DINIFTI_NIFTIIO_INCLUDE nifti1.h 
           PATHS ${INCLUDEDIR}
           PATH_SUFFIXES nifti niftilib libnifti
           DOC "Path to the NIfTIio library headers." )
FIND_PATH( DINIFTI_ZNZ_INCLUDE znzlib.h 
           PATHS ${INCLUDEDIR} 
           PATH_SUFFIXES znz znzlib libznz nifti libnifti niftilib
           DOC "Path to the znzlib headers." )
FIND_LIBRARY( DINIFTI_NIFTIIO_LIB niftiio
              PATHS ${LIBDIR}
              PATH_SUFFIXES nifti libnifti niftilib
              DOC "Location of the NIfTIio library." )
FIND_LIBRARY( DINIFTI_ZNZ_LIB znz
              PATHS ${LIBDIR}
              PATH_SUFFIXES znz libznz znzlib
              DOC "Location of the znz library." )

# platform dependent stuff
INCLUDE( TestBigEndian )
TEST_BIG_ENDIAN(DINIFTI_IS_BIG_ENDIAN)

IF(DINIFTI_IS_BIG_ENDIAN)
  # do whatever is necessary if system if big endian
  ADD_DEFINITIONS( -DARCHITECTURE=BIG_ENDIAN_ARCHITECTURE )

ELSE(DINIFTI_IS_BIG_ENDIAN)
  # for the little ones
  ADD_DEFINITIONS( -DARCHITECTURE=LITTLE_ENDIAN_ARCHITECTURE )

ENDIF(DINIFTI_IS_BIG_ENDIAN)

IF(APPLE)
  # put MacOS X specific configuration here
  ADD_DEFINITIONS( -DOSX -DOS=OSX )
ELSEIF(${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
  # put Linux/Unix specific stuff here
  ADD_DEFINITIONS( -DLINUX -DOS=LINUX )
ENDIF(APPLE)

ADD_DEFINITIONS( -DHAVE_ZLIB )

INCLUDE(CheckTypeSize)
CHECK_TYPE_SIZE(short SHORTSIZE)
ADD_DEFINITIONS( -DSHORTSIZE=${SHORTSIZE}*8)
CHECK_TYPE_SIZE(int INTSIZE)
ADD_DEFINITIONS( -DINTSIZE=${INTSIZE}*8)
CHECK_TYPE_SIZE(long LONGSIZE)
ADD_DEFINITIONS( -DLONGSIZE=${LONGSIZE}*8)

# add all include paths
INCLUDE_DIRECTORIES( ${CMAKE_SOURCE_DIR}/include 
                     ${DINIFTI_CTN_INCLUDE} 
                     ${DINIFTI_NIFTIIO_INCLUDE} )


# Configure default install path
IF( NOT DEFINED BINDIR )
  SET( BINDIR ${PREFIX}/bin CACHE PATH "Where the executable goes.")
ENDIF( NOT DEFINED BINDIR )

# dinifti
ADD_EXECUTABLE(dinifti src/dinifti.cc src/dicomInfo.cc src/niftiout.cc)
TARGET_LINK_LIBRARIES( dinifti ${DINIFTI_CTN_LIB}
                               ${DINIFTI_NIFTIIO_LIB}
                               ${DINIFTI_ZNZ_LIB} 
				z )

# dicomhead
ADD_EXECUTABLE(dicomhead src/dicomhead.cc src/dicomInfo.cc)
TARGET_LINK_LIBRARIES( dicomhead ${DINIFTI_CTN_LIB} )

# Optional "OPTIMIZED" and "DEBUG" modes

IF(OPTIMIZED)
  ADD_DEFINITIONS( -O )
  SET_TARGET_PROPERTIES( dinifti dicomhead PROPERTIES LINK_FLAGS -O )
ENDIF(OPTIMIZED)

IF(DEBUG)
  IF(APPLE)
    ADD_DEFINITIONS(-g)
    SET_TARGET_PROPERTIES( dinifti dicomhead PROPERTIES LINK_FLAGS -g )
  ELSEIF(${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
    ADD_DEFINITIONS(-ggdb)
    SET_TARGET_PROPERTIES( dinifti dicomhead PROPERTIES LINK_FLAGS -ggdb )
  ENDIF(APPLE)
ENDIF(DEBUG)

# configure where to install binaries
INSTALL(TARGETS dinifti dicomhead
    RUNTIME DESTINATION ${BINDIR} COMPONENT RuntimeLibraries )

# install manpages
FILE( GLOB DINIFTI_MANPAGES "man/*.1" )
INSTALL( FILES ${DINIFTI_MANPAGES} DESTINATION ${DINIFTI_MAN_DIR} )

