//  $Id: dicomhead.cc,v 1.4 2006-08-15 21:30:17 valerio Exp $	

//****************************************************************************
//
// Modification History (most recent first)
// mm/dd/yy  Who  What
//
// 11/28/05  VPL  
//
//****************************************************************************

#include "dicomhead.h"
#include "dicomInfo.h"
#include <string.h>

int main(int argc, char **argv)
{
    if (argc < 2)
    {
      cerr << "Usage: dicomhead <input file> [-extra]" << endl << endl;
      exit(1);
    }

    int extra = 0;
    if ( (argc == 3) && (strcmp(argv[2],"-extra") == 0) )
      extra = 1;

    DICOMImage *dcmImg = new DICOMImage(argv[1], false, false);
    if (*dcmImg)
    {
        // Dump the header
		
        DCM_OBJECT *object;
	unsigned long options = DCM_ORDERLITTLEENDIAN | DCM_FORMATCONVERSION;

	printf("DICOM File: %s\n", argv[1]);
	if ( DCM_OpenFile(argv[1], options, &object) != DCM_NORMAL )
	{
	    DCM_CloseObject(&object);
	    COND_PopCondition(TRUE);
	    if (DCM_OpenFile(argv[1], options | DCM_PART10FILE, &object) != DCM_NORMAL )
	    {
	        // Shouldn't happen since we've already opened it once
				
	        COND_DumpConditions();
		exit(1);
	    }
	}
		
	if ( DCM_DumpElements(&object, 0) != DCM_NORMAL )
	{
	    COND_DumpConditions();
	    exit(1);
	}
		
	DCM_CloseObject(&object);

	if ( extra == 1 )
	{
	    // Append a couple of extra info's to the text file

	    // To compute Z FOV (in mm) we need to know the spacing between slicing
	    // and slice thickness. If the spacing is 0 (contigous) use only
	    // thickness.

	    float zfov = 0.;

	    if ( (dcmImg->SliceSpacing() < 0.0001) && (dcmImg->SliceSpacing() > -0.0001) )
	      zfov = dcmImg->NumSlices() * dcmImg->SliceThickness();
	    else
	      zfov = (dcmImg->NumSlices() - 1) * dcmImg->SliceSpacing() + dcmImg->SliceThickness();
	    
	    cout << endl << "Z-Direction Cosines: {"
		 << dcmImg->SagNorm() << ", " << dcmImg->CorNorm() << ", "
		 << dcmImg->TraNorm() << "}" << endl
		 << "Number of slices: " <<  dcmImg->NumSlices() << endl
		 << "Z FOV (in mm): " << zfov << endl;
	}
    }
}

